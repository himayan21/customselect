import React, { Component } from "react";
import "./App.css";
import SelectCustom from "./SelectCustom";
class App extends Component {
  state = {
    isMulti: false
  };
  radioChange = e => {
    let isMulti = false;
    if (e.target.value === "single") isMulti = false;
    else isMulti = true;
    this.setState({ isMulti });
  };
  render() {
    return (
      <div className="App">
        <div className="radio-select">
          <div className="radio-option">
            <input
              type="radio"
              value="single"
              checked={this.state.isMulti === false}
              onChange={this.radioChange}
            />{" "}
            Single
          </div>
          <div className="radio-option">
            <input
              type="radio"
              value="multi"
              checked={this.state.isMulti === true}
              onChange={this.radioChange}
            />{" "}
            Multi
          </div>
        </div>
        <SelectCustom
          numberArray={["one", "two", "three", "four", "five"]}
          isMulti={this.state.isMulti}
        />
      </div>
    );
  }
}

export default App;
