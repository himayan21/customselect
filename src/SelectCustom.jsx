import React, { Component } from "react";
class SelectCustom extends Component {
  state = {
    options: [],
    selectedValue: [],
    show: false
  };
  componentDidMount() {
    this.setState({ options: this.props.numberArray });
    window.addEventListener("click", e => {
      if (!document.getElementById("option-box").contains(e.target)) {
        this.dontShow();
      }
    });
  }
  show = () => {
    this.setState({ show: true });
  };
  dontShow = () => {
    this.setState({ show: false });
  };
  handleChange = e => {
    let options = e.target.options;
    let value = [];
    if (this.props.isMulti === true) value = [...this.state.selectedValue];
    for (let i = 1, l = options.length; i < l; i++) {
      if (options[i].selected) {
        if (value.includes(options[i].value) === false)
          value.push(options[i].value);
      } else {
        if (value.includes(options[i].value) === true) {
          let removeSelection = options[i].value;
          let index = value.indexOf(removeSelection);
          value.splice(index, 1);
        }
      }
    }
    this.setState({ selectedValue: value });
  };
  deleteSelection = e => {
    let selectedValue = [...this.state.selectedValue];
    let deleteOption = e.target.value;
    let index = selectedValue.indexOf(deleteOption);
    if (index > -1) {
      selectedValue.splice(index, 1);
      this.setState({ selectedValue });
    }
  };
  updateOptions = e => {
    let input = e.target.value;
    let options = [];
    for (let i = 0, l = this.props.numberArray.length; i < l; i++) {
      if (this.props.numberArray[i].includes(input)) {
        options.push(this.props.numberArray[i]);
      }
    }
    this.setState({ options });
  };
  render() {
    return (
      <div className="form-group option-box" id="option-box">
        <div className="dropdown">
          <div className="selected-options">
            {this.state.selectedValue.map(element => (
              <div className="selected-option">
                <div className="option-text">{element}</div>
                <div className="delete">
                  <button
                    className="delete-button"
                    value={element}
                    onClick={this.deleteSelection}
                  >
                    ✕
                  </button>
                </div>
              </div>
            ))}
          </div>

          <div
            className={
              this.props.isMulti === false &&
              this.state.selectedValue.length > 0
                ? "invisible"
                : "input-block"
            }
          >
            <input
              placeholder="type your query here"
              className="form-control input-box"
              type="text"
              onChange={this.updateOptions}
              onFocus={this.show}
            />
          </div>
          {this.state.show ? (
            <i onClick={this.dontShow} className="fas fa-angle-double-up" />
          ) : (
            <i onClick={this.show} className="fas fa-angle-double-down" />
          )}
        </div>
        <div className={this.state.show ? "active" : "inactive"}>
          <select
            className="form-control option-collection"
            size="3"
            multiple={this.props.isMulti}
            value={this.state.selectedValue}
            onChange={this.handleChange}
            onBlur={this.dontShow}
          >
            <option hidden disabled>
              Select an option
            </option>
            {this.state.options.map(element => (
              <option value={element}>{element}</option>
            ))}
          </select>
        </div>
      </div>
    );
  }
}

export default SelectCustom;
